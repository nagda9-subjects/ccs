%% Inverted pendulum control and reference tracking (servo)


%% Nonlinear model - parameters (A)
% No friction

syms t x v phi omega

% model parameters
M = 0.5;
m = 0.2;
l = 1;
g = 9.8;
b = 0;

q = 4*(M+m) - 3*m*cos(phi)^2;

f_sym = [
    v
    (4*m*l*sin(phi)*omega^2 - 1.5*m*g*sin(2*phi) -4*b*v) / q
    omega
    3*(-m*l*sin(2*phi)*omega^2 / 2 + (M+m)*g*sin(phi) + b*cos(phi)*v) / (l*q)
    ];

g_sym = [
    0
    4*l
    0
    -3*cos(phi)
    ] / (l*q);

f_ode = matlabFunction(f_sym, 'vars', {t , [ x ; v ; phi ; omega ]});
g_ode = matlabFunction(g_sym, 'vars', {t , [ x ; v ; phi ; omega ]});


%% Linearized model around the unstable equilibrium point

% model parameters
M = 0.5;
m = 0.2;
l = 1;
g = 9.8;
b = 0;

A = [
    0                     1                             0  0
    0      -(4*b)/(4*M + m)            -(3*g*m)/(4*M + m)  0
    0                     0                             0  1
    0   (3*b)/(l*(4*M + m))   (3*g*(M + m))/(l*(4*M + m))  0
    ];

B = [
    0
    4/(m+4*M)
    0
    -3/(m+4*M)/l
    ];

C = [
    1 0 0 0
    0 0 1 0
    ];

D = [ 0 ; 0 ];

%% Task 1: Create the simulink model ipend2_sim

open_system ipend2_sim

% Initial value can be set in the integrator block, eg. x0 = [2;0;0.3;0];

% Hint: you can use set_param to set the parameter of a gain in the simulink model:
% eg. set_param('ipend2_sim/<BLOCKNAME>','<PARAMETERNAME (eg. Gain)>',csb_num2str(<VALUE>))


%% Task 2: Design a pole placement controller and an observer gain

% a) Translate poles into [-4 -3 -2 -1] (or any stable configuration)

% help place
% K_pp = ?
% set_param

% b) Minimizes the given functional (LQR)

% help lqr
% K_lqr = ?
% set_param


%% Task 3: Apply the state feedback to the system

% Hint: use a manual switch to decide which gain to apply


%% Task 4: Add a saturation block to the input
% Experiment with the saturation parameters (low saturation and far from
% zero initial state will result in unreachable states)

% Try initial value: x0 = [2;0;0.3;0];
% With saturation values: 20, 10, 5, 1

%% Task 5: design a state observer for the linearized system

% Hint: as discussed during seminars, state observer design is practically
% a pole placement design for a special A and B matrix
% Hint2: setting high-value poles leads to faster convergence (but not
% everything is possible in real systems)
% Hint3: if the observer is started from different x0, transient lasts
% longer - might be a problem for the controller if too long
% Dont forget to use set_params

% help place



%% Task6: connect everything together
% Experiment with different input signals

%% Simulate simulink results

% open the model (open_system <modelname>)
open_system ipend2_sim

% simulate
out = sim('ipend2_sim');

%% Visualize external control force

t = out.simx.Time;
x = out.simx.Data;
u = out.simu.Data;
xobs = out.xobs.Data;
r = out.simr.Data;

% Show a video of the pendulum
ipend_simulate_0(t,x,u, r)


%% HELPER FUNCTION FOR SET_PARAM
function [res]=csb_num2str(num)
    res = regexprep(mat2str(num), {'\[', '\]', '\s+'}, {'\[', '\]', ','});
end
